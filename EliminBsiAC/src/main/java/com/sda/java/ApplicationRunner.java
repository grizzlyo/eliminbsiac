package com.sda.java;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class ApplicationRunner
{
    public static void main( String[] args )
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduceti sirul initial: ");
        String sir = s.nextLine();

        EliminBsiAC elimin = new EliminBsiAC();

        System.out.println("Afisez sirul final dupa eliminarea caracterului 'b' si/sau literelor 'ac' prin 2 iteratii :" + "\n" + elimin.rezultat(sir));
        System.out.println("Afisez sirul final dupa eliminarea caracterului 'b' si/sau literelor 'ac', printr-o singura iteratie : " + "\n" + elimin.returnWithoutBandAC(sir));
    }
}
