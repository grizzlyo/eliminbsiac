package com.sda.java;


public class EliminBsiAC {
    public String rezultat(String sirInitial) {
        String sirFinal = null;
        if (sirInitial.contains("b") || sirInitial.contains("ac")) {
            String nou = sirInitial.replace("b", "");
            sirFinal = nou.replace("ac", "");
        }
        return sirFinal;
    }

    public String returnWithoutBandAC(String sir) {
        StringBuilder builder = new StringBuilder(sir);

        for (int i = 0; i < builder.length(); i++) {
            if (builder.charAt(i) == 'b') {
                builder.deleteCharAt(i);
                i--;
            } else if (builder.charAt(i) == 'a' && builder.charAt(i + 1) == 'c') {
                builder.delete(i, i + 2);
                i--;
            }
        }
        String rezultat = builder.toString();
        return rezultat;

    }

}

