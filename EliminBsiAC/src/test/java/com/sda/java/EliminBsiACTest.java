package com.sda.java;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple ApplicationRunner.
 */
public class EliminBsiACTest
{
    @Test
    public void metoda1Test()
    {
        System.out.println("metoda1Test a fost apelata! ");
        EliminBsiAC elimin = new EliminBsiAC();
        String initial = elimin.rezultat("aab");
        String expected = "aa";
        Assert.assertEquals(expected, initial);

    }

    @Test
public void metoda2Test(){
    System.out.println("metoda2Test a fost apelata!");
    EliminBsiAC elimin = new EliminBsiAC();

    String initial = elimin.returnWithoutBandAC("bacbc");
    String expected = "c";
    

    Assert.assertEquals(expected,initial);
}
}
